Package.describe({
  name: 'hsed:stub-collections',
  version: '1.0.10',
  summary: 'Stub out Meteor collections with in-memory local collections.',
  documentation: 'README.md',
  git: 'https://bitbucket.org/hsedevelopment/meteor-stub-collections',
  debugOnly: true,
});

Npm.depends({
  'sinon': '7.3.2',
});

Package.onUse(function onUse(api) {
  api.versionsFrom('2.5');
  api.use([
    'ecmascript',
    'mongo',
    'tmeasday:check-npm-versions@1.0.2',
  ]);
  api.addFiles('peer-deps.js');
  api.mainModule('index.js');
});

Package.onTest(function onTest(api) {
  api.use([
    'ecmascript',
    'mongo',
    'hsed:stub-collections',
    'meteortesting:mocha@2.0.3',
    'aldeed:collection2@2.10.0 || 3.2.1',
  ]);
  api.mainModule('tests.js');
});
