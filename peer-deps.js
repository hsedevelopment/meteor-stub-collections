import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';

const deps = {
  'simpl-schema': '1.13.1',
  ...Meteor.isPackageTest && {
    'chai': '4.2.0',
  },
};

checkNpmVersions(deps, 'hse-stub-collections');
